# Code snippets

The `pre` files are older JavaScript syntax.

The `post` files are newer JavaScript syntax.

This is a list of what you will find in each pair of pre/post files.

| File group   | Topic                                        | Older syntax                    | Modern syntax                     |
| ------------ | -------------------------------------------- | ------------------------------- | --------------------------------- |
| **01 files** | `var` vs `let`                               | [[older](./snippets/01-pre.js)] | [[modern](./snippets/01-post.js)] |
| **02 files** | `var` vs `const`                             | [[older](./snippets/02-pre.js)] | [[modern](./snippets/02-post.js)] |
| **03 files** | How does `const` work?                       | [[older](./snippets/03-pre.js)] | [[modern](./snippets/03-post.js)] |
| **04 files** | How does `const` work?                       | [[older](./snippets/04-pre.js)] | [[modern](./snippets/04-post.js)] |
| **05 files** | Destructuring objects                        | [[older](./snippets/05-pre.js)] | [[modern](./snippets/05-post.js)] |
| **06 files** | Destructuring arrays                         | [[older](./snippets/06-pre.js)] | [[modern](./snippets/06-post.js)] |
| **07 files** | Rest operator in assignment                  | [[older](./snippets/07-pre.js)] | [[modern](./snippets/07-post.js)] |
| **08 files** | Destructuring object arguments               | [[older](./snippets/08-pre.js)] | [[modern](./snippets/08-post.js)] |
| **09 files** | Rest operator in arguments                   | [[older](./snippets/09-pre.js)] | [[modern](./snippets/09-post.js)] |
| **10 files** | Spreading arguments                          | [[older](./snippets/10-pre.js)] | [[modern](./snippets/10-post.js)] |
| **11 files** | Rest and spread example                      | [[older](./snippets/11-pre.js)] | [[modern](./snippets/11-post.js)] |
| **12 files** | Default parameter values                     | [[older](./snippets/12-pre.js)] | [[modern](./snippets/12-post.js)] |
| **13 files** | Interpolation with template literas          | [[older](./snippets/13-pre.js)] | [[modern](./snippets/13-post.js)] |
| **14 files** | Multiline template literals                  | [[older](./snippets/14-pre.js)] | [[modern](./snippets/14-post.js)] |
| **15 files** | Class declaration                            | [[older](./snippets/15-pre.js)] | [[modern](./snippets/15-post.js)] |
| **16 files** | Method declaration                           | [[older](./snippets/16-pre.js)] | [[modern](./snippets/16-post.js)] |
| **17 files** | Arrow function I                             | [[older](./snippets/17-pre.js)] | [[modern](./snippets/17-post.js)] |
| **18 files** | Arrow function II                            | [[older](./snippets/18-pre.js)] | [[modern](./snippets/18-post.js)] |
| **19 files** | Arrow function III                           | [[older](./snippets/19-pre.js)] | [[modern](./snippets/19-post.js)] |
| **20 files** | `for-of` loop for looping over array entries | [[older](./snippets/20-pre.js)] | [[modern](./snippets/20-post.js)] |
