// ESv1: concatenating strings
function printName(person) {
  var fullName = person.firstName +
                 " " +
                 person.lastName;
  console.log(fullName);
}

var person = {
  firstName: "Curtis",
  lastName: "Schlak",
};
printName(person);
