// ESv1: the rest of the arguments
function printNameAndInfo(name) {
  var info = Array.prototype.slice.call(arguments, 1);
  console.log("name", name);
  console.log("info", info);
}

printNameAndInfo("Curtis", 75, "Houston");
