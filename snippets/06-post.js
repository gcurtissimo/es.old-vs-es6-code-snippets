// ESv6: Destructuring array entries
const a = ["First", "Second", "Third"];

const [primero, segundo, tercero] = a;
console.log(primero, segundo, tercero);

const b = [];
const [first] = b;
console.log(first);
