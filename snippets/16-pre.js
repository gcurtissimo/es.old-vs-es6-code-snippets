// ESv1: Adding a method to a "class"
var Delay = function (delay, reaction) {
  this.delay = delay;
  this.reaction = reaction;
};

Delay.prototype.haveReaction = function() {
  var reaction = this.reaction;
  setTimeout(function() {
    console.log(reaction);
  }, this.delay);
}

var o = new Delay(1000, "WOW!");
o.haveReaction();
