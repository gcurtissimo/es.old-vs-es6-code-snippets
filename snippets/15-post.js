// ESv6: Declaring a class for real
class Delay {
  constructor(delay, reaction) {
    this.delay = delay;
    this.reaction = reaction;
  }
}

const o = new Delay(1000, "WOW!");
console.log(o.delay);
console.log(o.reaction);
