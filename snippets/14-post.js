// ESv1: backtick multiline strings
function htmlMessage(message) {
  const html = `
    <div class="message">
      <span class="alert">
        ${message}
      </span>
    </div>
  `;
  return html.trim();
}

const msg = htmlMessage("DANGER!");
console.log(msg);
