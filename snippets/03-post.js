// ESv6: how does const work?
const o = {"one": 1, "two": 2};
o["one"] = "I"
o["two"] = "II";

const a = [1, 2, 3];
a[1] = 9;

console.log(o);
console.log(a);
