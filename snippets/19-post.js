let noArgs = () => console.log("I have no args");

// ESv6: Special case of one arg needs no
//       parentheses
let oneArg = a => console.log("I have one arg.");

let twoArgs = (a, b) => console.log("I have two args");
