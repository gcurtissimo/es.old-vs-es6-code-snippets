// ESv1: create an array that's
//       a reverse of the argument
function recursiveReverse(a) {
  if (a.length === 0) {
    return [];
  }
  var first = a[0];
  var rest = a.slice(1);
  var rev = recursiveReverse(rest);
  rev.push(first);
  return rev;
}

var a = [1, 2, 3, 4, 5];
console.log(recursiveReverse(a));
