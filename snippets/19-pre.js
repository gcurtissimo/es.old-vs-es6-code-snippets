var noArgs = function() {
  console.log("I have no args");
};

var oneArg = function(a) {
  console.log("I have one arg.");
};

var twoArgs = function (a, b) {
  console.log("I have two args");
};
