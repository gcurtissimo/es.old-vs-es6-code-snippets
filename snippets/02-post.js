// ESv6: const is block scoped
if (true) {
  const i = 0;
}
console.log(i);
