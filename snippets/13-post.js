// ESv6: using template literals
function printName({firstName, lastName}) {
  const fullName = `${firstName} ${lastName}`;
  console.log(fullName);
}

const person = {
  firstName: "Curtis",
  lastName: "Schlak",
};
printName(person);
