// ESv6: Declaring a class for real
class Delay {
  constructor(delay, reaction) {
    this.delay = delay;
    this.reaction = reaction;
  }

  haveReaction() {
    const reaction = this.reaction;
    setTimeout(function() {
      console.log(reaction);
    }, this.delay);
  }
}

const o = new Delay(1000, "WOW!");
o.haveReaction();
