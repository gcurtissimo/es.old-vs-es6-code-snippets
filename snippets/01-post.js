// ESv6: let is block scoped
if (true) {
  let i = 0;
}
console.log(i);
