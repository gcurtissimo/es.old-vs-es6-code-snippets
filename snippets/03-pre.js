// ESv1: var is really relaxed
var o = {"one": 1, "two": 2};
o["one"] = "I"
o["two"] = "II";

var a = [1, 2, 3];
a[1] = 9;

console.log(o);
console.log(a);
