// ESv6: Destructuring objects
const o = {
  name: "Curtis",
  heightInInches: 75,
};

const { name, foo } = o;
console.log("name:", name);
console.log("foo:", foo);
