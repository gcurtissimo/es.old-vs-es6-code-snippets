// ESv1: the spreading of arguments
var dateParts = [2020, 03, 10];
var date = new Date(
  dateParts[0],
  dateParts[1],
  dateParts[2]
);
console.log("date     ", date);

// ESv1: adding more entries to an
//       existing array
dateParts.push(14);
dateParts.push(8)
var date = new Date(
  dateParts[0],
  dateParts[1],
  dateParts[2],
  dateParts[3],
  dateParts[4],
);
console.log("date+hour", date);
