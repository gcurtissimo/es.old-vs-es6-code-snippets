// ESv1: parameter properties
function printName(person) {
  var name = person.name;
  console.log(name);
}

var o = {
  name: "Curtis",
  shoeSize: 10.5,
};

printName(o);
