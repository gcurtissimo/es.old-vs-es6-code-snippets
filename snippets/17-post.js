class Delay {
  constructor(delay, reaction) {
    this.delay = delay;
    this.reaction = reaction;
  }

  haveReaction() {
    // ESv6: arrow functions do NOT have their
    //       own "this" and uses the surrounding
    //       "this"
    const cb = () => console.log(this.reaction);
    setTimeout(cb, this.delay);
  }
}

const o = new Delay(1000, "WOW!");
o.haveReaction();
