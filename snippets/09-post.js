// ESv6: the rest of the arguments
function printNameAndInfo(name, ...info) {
  console.log("name", name);
  console.log("info", info);
}

printNameAndInfo("Curtis", 75, "Houston");
