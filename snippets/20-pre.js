// ESv1: iterating over entries in an
//       array
function printEachItem(arr) {
  for (var i = 0; i < arr.length; i += 1) {
    var item = arr[i];
    console.log(item);
  }
}

printEachItem([1, 2, 3]);
