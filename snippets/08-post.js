// ESv6: destructuring object parameters
function printName({name}) {
  console.log(name);
}

const o = {
  name: "Curtis",
  shoeSize: 10.5,
};

printName(o);
