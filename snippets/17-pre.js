var Delay = function (delay, reaction) {
  this.delay = delay;
  this.reaction = reaction;
};

Delay.prototype.haveReaction = function() {
  var reaction = this.reaction;

  // ESv1: functions declared with "function"
  //       always have their own "this"
  const cb = function() {
    console.log(reaction);
  };
  setTimeout(cb, this.delay);
}

var o = new Delay(1000, "WOW!");
o.haveReaction();
