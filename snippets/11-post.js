// ESv6: create an array that's
//       a reverse of the argument
function recursiveReverse(first, ...rest) {
  if (first === undefined) {
    return [];
  }
  return [...recursiveReverse(...rest), first];
}

const a = [1, 2, 3, 4, 5];
console.log(recursiveReverse(...a));
