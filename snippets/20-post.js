// ESv6: iterating over entries in an
//       array
function printEachItem(arr) {
  for (let item of arr) {
    console.log(item);
  }
}

printEachItem([1, 2, 3]);
