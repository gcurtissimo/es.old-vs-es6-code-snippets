// ESv1: Declaring a "class"
var Delay = function (delay, reaction) {
  this.delay = delay;
  this.reaction = reaction;
};

var o = new Delay(1000, "WOW!");
console.log(o.delay);
console.log(o.reaction);
