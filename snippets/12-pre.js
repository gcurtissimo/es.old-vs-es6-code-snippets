// ESv1: handling unprovided arguments
function example(required, optional) {
  optional = optional || "OPTIONAL";
  console.log(required, optional);
}

example("ONE", "TWO");
example("ONE");
