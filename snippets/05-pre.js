// ESv1: Getting property values
var o = {
  name: "Curtis",
  heightInInches: 75,
};

var name = o.name;
var foo = o.foo;
console.log("name:", name);
console.log("foo:", foo);
