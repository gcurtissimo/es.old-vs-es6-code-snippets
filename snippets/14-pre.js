// ESv1: building multiline strings
function htmlMessage(message) {
  var html = ['<div class="message">',
              '  <span class="alert">',
              '    ' + message,
              '  </span>',
              '</div>'];
  return html.join('\n');
}

var msg = htmlMessage("DANGER!");
console.log(msg);
