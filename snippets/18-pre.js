// ESv5: functional callbacks
var nums = [1, 2, 3, 4, 5];
var squares = nums.map(function (n) {
  var square = n * n;
  return square;
});
console.log("squares", squares);
