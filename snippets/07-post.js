// ESv6: Getting the rest of the entries
const a = ["First", "Second", "Third"];

const [primero, ...rest] = a; // Rest operator
console.log(rest);
