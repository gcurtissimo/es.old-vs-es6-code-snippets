// ESv6: functional callbacks
const nums = [1, 2, 3, 4, 5];
const squares = nums.map(n => {
  const square = n * n;
  return square;
});
console.log("squares", squares);

// Arrow function without curly braces
// have an implicit return statement but
// can be only one line
const also = nums.map(n => n * n);
console.log("also   ", also);
