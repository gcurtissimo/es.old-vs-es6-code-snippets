// ESv6: the spreading of arguments
let dateParts = [2020, 03, 10];
let date = new Date(...dateParts);
console.log("date     ", date);

// ESv6: spreading into a new array
//       and assigning it back to
//       the same variable
dateParts = [...dateParts, 14, 8];
date = new Date(...dateParts);
console.log("date+hour", date);
