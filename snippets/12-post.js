// ESv6: default arguments
function example(required, optional="OPTIONAL") {
  console.log(required, optional);
}

example("ONE", "TWO");
example("ONE");
