// ESv1: Getting array entries
var a = ["First", "Second", "Third"];

var primero = a[0];
var segundo = a[1];
var tercero = a[2];
console.log(primero, segundo, tercero);

var b = [];
var first = b[0];
console.log(first);
